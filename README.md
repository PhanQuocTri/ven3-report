# README #

VEN with updated Report functionality to LivinglabLM Server

### What is this repository for? ###

Read values from powermeters and display the according results on EPRI VTN, and later Siemens VTN

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

Use available GUI to run VEN
Input VEN name, ID, VTN address and license file
Connect and read to the DB

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

Add DB class
Add TextRecord Class
Add Report_Gateway_DB Class
Add ReportAction Class

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

Tri Phan 
Email: c2gg4@unb.ca
Phone: (506)470-2120
﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oadr2b_ven.ven
{
    class A_DB
    {

        public string m_connectionString;
        public string m_commandString;
        public SqlDataReader m_readerRead;
        public SqlCommand m_commandCom;
        public SqlTransaction m_transTrans;

        private float m_device_Power, m_device_Energy;
        private int m_device_State, m_device_ID;
        private DateTime m_device_DAT;

        public void ConnectSQLDB(string connectionStr, string commandStr)
        {

            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                // use SQL command here
                try
                {
                    conn.Open();
                    conn.Close();
                    conn.Dispose();
                }
                catch (Exception ex)
                {
                }

            }
        }

        public float ReadSQLDB(string connectionStr, string commandStr)
        {
            SqlDataReader reader = null;
            SqlCommand command = new SqlCommand();


            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                try
                {
                    command.CommandText = commandStr;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.Connection.Open();
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        m_device_ID = int.Parse(reader["ID"].ToString());
                        m_device_Power = float.Parse(reader["Power"].ToString());
                        m_device_Energy = float.Parse(reader["Energy"].ToString());
                        m_device_State = int.Parse(reader["State"].ToString());
                        m_device_DAT = DateTime.Parse(reader["DateAndTime"].ToString());
                    }
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
                catch (Exception ex)
                {
                    return 1000;
                }
            }
            if (m_device_Power != 0)
            { return m_device_Power; }
            else
            { return m_device_Power; }
        }


        public void WriteSQLDB(string connectionStr, string commandStr)
        {
            SqlCommand command = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                try
                {
                    command.CommandText = commandStr;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
                catch (Exception ex)
                {
                }
            }
        }

        /**********************************************************/

        public int device_ID
        {
            get { return m_device_ID; }
        }

        /**********************************************************/

        public float device_Power
        {
            get { return m_device_Power; }
        }

        /**********************************************************/

        public float device_Energy
        {
            get { return m_device_Energy; }
        }

        /**********************************************************/

        public int device_state
        {
            get { return m_device_State; }
        }

        /**********************************************************/

        public DateTime device_DAT
        {
            get { return m_device_DAT; }
        }

    }
}

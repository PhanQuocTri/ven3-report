﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace oadrlib.lib.oadr2b
{
    class ASub_DB
    {

        public string m_connectionString;
        public string m_commandString;
        public string m_atm;
        public string m_newatm;

        public SqlDataReader m_readerRead;
        public SqlCommand m_commandCom;
        public SqlTransaction m_transTrans;

        private string m_device_Power, m_device_Energy;
        private string m_device_State, m_device_ID;
        private DateTime m_device_DAT;

        public void ConnectSQLDB(string connectionStr, string commandStr)
        {

            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                // use SQL command here
                try
                {
                    conn.Open();
                    conn.Close();
                    conn.Dispose();
                }
                catch (Exception ex)
                {
                }

            }
        }

        public void ReadSQLDB(string connectionStr, string commandStr)
        {
            SqlDataReader reader = null;
            SqlCommand command = new SqlCommand();


            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                try
                {
                    command.CommandText = commandStr;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.Connection.Open();
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {                     
                        m_device_ID = (reader["Acuvim_II_ID"].ToString());
                        m_device_Power = (reader["Power_P1"].ToString());
                        m_device_DAT = DateTime.Parse(reader["dt"].ToString());
                        // m_device_State = (reader["ReadingID"].ToString());
                        // m_device_Energy = (reader["Energy_Q1"].ToString());
                    }
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
                catch (Exception ex)
                {
                }
            }
        }


        public void WriteSQLDB(string connectionStr, string commandStr)
        {
            SqlCommand command = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(connectionStr))
            {
                try
                {
                    command.CommandText = commandStr;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                    command.Connection.Dispose();
                }
                catch (Exception ex)
                {
                }
            }
        }

        /**********************************************************/

        public int device_ID
        {
            get { return int.Parse(m_device_ID); }
        }

        /**********************************************************/

        public float device_Power
        {
            get { return float.Parse(m_device_Power); }
        }

        /**********************************************************/

        public float device_Energy
        {
            get { return float.Parse( m_device_Energy); }
        }

        /**********************************************************/

        public int device_state
        {
            get { return int.Parse(m_device_State); }
        }

        /**********************************************************/

        public DateTime device_DAT
        {
            get
            {
                return m_device_DAT;
            }
        }

    }
}

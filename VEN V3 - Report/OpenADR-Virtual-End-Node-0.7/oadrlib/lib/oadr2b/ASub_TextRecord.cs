﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Globalization;

namespace oadrlib.lib.oadr2b
{
    class ASub_TextRecord
    {
        /// <summary>
        /// Log File Creater: Source: http://stackoverflow.com/questions/20185015/how-to-write-log-file-in-c
        /// </summary>
        /// 
        private string m_exePath = string.Empty;

        public void LogWriter(string[] logMessage)
        {
            LogWrite(logMessage);
        }

        public void LogWrite(string[] logMessage)
        {
            m_exePath = @"H:\Record Keeping\VEN Modified with DB\";
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "RecordReport1.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void Log(string[] logMessage, TextWriter txtWriter)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");

            try
            {
                txtWriter.Write("\r\n");
                txtWriter.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7}", logMessage[0], logMessage[1], logMessage[2], logMessage[3], logMessage[4], logMessage[5], logMessage[6], logMessage[7]);

            }
            catch (Exception ex)
            {
            }
        }
    }
}
